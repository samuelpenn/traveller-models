# Traveller Logos

These are some logos for various companies and organisations in my Traveller campaign.

Several of the files use a Vilani font, which can be downloaded from here:
http://www.trisen.com/sol/default.asp?topic=10&page=123

License for the logos is CC0
