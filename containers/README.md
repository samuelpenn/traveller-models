# Shipping Containers

A collection of shipping containers of various types.

![Hexagonal Container](cargo_hexagonal.png "1dT Container")

![4D Container](cargo-4C.png "4dT Container")

License: CC0
